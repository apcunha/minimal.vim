" Name: Minimal
" Author: António Pedro Cunha
" URL: https://gitlab.com/apcunha/minimal.vim

highlight clear

if exists('syntax_on')
    syntax reset
endif

let g:colors_name = 'minimal'
